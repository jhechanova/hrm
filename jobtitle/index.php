<?php
include('../header.php');
?>
<?php
$server_name = 'localhost';
$username = 'root';
$user_password = '';
$db_name = 'hrm';
$table = 'job_title';
$conn = new mysqli($server_name,$username,$user_password,$db_name);
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}
?>
    <div class="container">
        <center><table class="table table-bordered table-hover">
                <thead class="thead">
                <p align="center">
                <th width="5%"> ID# </th>
                <th width="30%"> Job Title </th>
                <th width="70%"> Job Description </th>
                <th width="20%"> Status </th>
                <h1><p align="left">List of Job Title </p></h1>
                <p align="right"><a href="job_form.php"><input type="button" class="btn btn-primary" name="update" value="Add New Job Title"></a></p>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql = 'SELECT * FROM hrm.job_title ';
                $result = $conn->query($sql);
                if ($result && $result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                        echo "<tr><td align='center'><a href ='job_form.php?id=".$row['id']."&parent=".$_GET['parent']."&level=".$_GET['level']."&tab_id=".$_GET['tab_id']."'>". $row["id"]."</a></td>";
                        echo "<td>" .$row["job_title"]. "</td> ";
                        echo "<td>" . $row["job_description"]. "</td>";
                        echo "<td>". $row["status"]."</td>";
                    }
                } else {
                    echo "0 results";
                }
                $conn->close();
                ?>
                </tbody>

                <a href="job_form.php?id=<?php echo $result["id"]; ?>"></a>

            </table>
        </center>
    </div>
<?php
include('../footer.php');
?>