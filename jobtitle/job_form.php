<?php
include('../header.php');
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'job_title';
$conn = mysqli_connect($server_name,$username,$password,$db_name);
?>
<?php
$dataId = '';
$dataJobTitle = '';
$dataJobDescription = '';
$btnName = 'Save';
$formLabel = 'New';
$dataStatus = 'Active';
try {
    if (isset($_GET['id'])) {

        $sql = "SELECT * FROM hrm.job_title where id = '" . $_GET['id'] . "'";//var_dump($sql);
        $result = mysqli_query($conn, $sql);
        $data = mysqli_fetch_row($result);
        $dataId = $data['0'];
        $dataJobTitle = $data['1'];
        $dataJobDescription = $data['2'];
        $btnName = 'Update';
        $dataStatus = $data['3'];
        $formLabel = 'Edit';

    }
}catch (Exception $e){var_dump($e->getMessage());}
?>
    <div class="container">
        <form id = "job_update_form" data-parent="<?php echo $_GET['parent'];?>" data-level="<?php echo $_GET['level'];?>" data-tab_id="<?php echo $_GET['tab_id'];?>">
            <input type="hidden" id ="dataId" name="dataId" size="30" maxlength="30" value="<?php echo $dataId;?>"/>
            <h1><?= $formLabel ?> Form</h1>
            <br>
            <div class="form-group">
                <label for="tasks_name">Job Title:</label>
                <input type="text" class="form-control" required id = "job_title" autofocus  placeholder="Enter Job Title.." name="job_title" value="<?php echo $dataJobTitle;?>">
            </div>
            <div class="form-group">
                <label for="tasks_description">Job Description:</label>
                <input type="text" class ="form-control" required id = "job_description" placeholder="Enter Job Description.." name="job_description" value="<?php echo $dataJobDescription;?>">
            </div>
            <div class="form-group">
                <label for="status">Status:</label>
                <select class="custom-select custom-select-sm" id ="status" name="status">
                   <option <?= ($dataStatus == 'Active') ? 'selected' : '' ?> value="Active" selected>Active</option>
                   <option <?= ($dataStatus == 'Inactive') ? 'selected' : '' ?> value="Inactive">Inactive</option>
                </select>
            </div>
            <p align="right">
            <button type="submit" class="btn btn-primary" name="update" value="update"><?= $btnName ?></button>
                <input type="button" class="btn btn-secondary" value="Back" onclick="history.back()"></p>

        </form>
    </div>
<?php
include('../footer.php');
?>