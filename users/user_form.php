<?php
include('../header.php');
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'user';
// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<?php

$dataId = '';
$dataFirstName = '';
$dataLastName = '';
$dataUsername = '';
$dataEmail = '';
$dataPassword = '';
$dataPhoneNumber = '';
$dataJobTitle = '';
$btnName = 'Save';
$formLabel = 'New';
$dataPassword = 'required';


if (isset($_GET['id'])) {
    $sql = "SELECT * FROM hrm.user where id = " . $_GET['id'];
    $result = $conn->query($sql);
    $data = mysqli_fetch_row($result);
    $dataId = $data['0'];
    $dataFirstName = $data['1'];
    $dataLastName = $data['2'];
    $dataUsername = $data['3'];
    $dataEmail = $data['4'];
    $dataPassword = $data['5'];
    $dataPhoneNumber = $data['6'];
    $dataJobTitle = $data['7'];
    $btnName = 'Update';
    $formLabel = 'Edit';
    $dataPassword = '';

}


?>


    <div class="container">
    <form id="user_edit_form" data-parent="<?php echo $_GET['parent'];?>" data-level="<?php echo $_GET['level'];?>" data-tab_id="<?php echo $_GET['tab_id'];?>">
        <input type="hidden" id = "dataId" name="dataId" value="<?php echo $dataId; ?>"/>
        <h1><?= $formLabel?> Form</h1>
        <br>

        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input type="text" class="form-control" placeholder="Enter First Name" autofocus required id="first_name" name="first_name" value="<?php echo $dataFirstName;?>"/>
        </div>
        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" class="form-control" placeholder="Enter Last Name" required id="last_name" name="last_name" value="<?php echo $dataLastName;?>"/>
        </div>
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" class="form-control" placeholder="Enter User Name" required id="username" name="username"   value="<?php echo $dataUsername;?>"/>
        </div>
        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" class="form-control" placeholder="Example@mail.com" required id="email" name="email" value="<?php echo $dataEmail;?>"/>
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control"  <?php echo $dataPassword;?> id="password" name="password"   value="<?php echo $dataPassword;?>"/>
        </div>
        <div class="form-group">
            <label for="password">Confirm Password:</label>
            <input type="password" class="form-control"  id="confirm_password" name="confirm_password"  value="<?php echo $dataPassword;?>"/>

        </div>
        <div class="form-group">
            <label for="phone_number">Phone Number:</label>
            <input type="text" class="form-control" required placeholder="Enter Phone Number" id="phone_number" name="phone_number" value="<?php echo $dataPhoneNumber;?>"/>
        </div>

        <div class="form-group">
            <label for="job_title">JobTitle</label>
            <select type="text" class="form-control"  id="job_title" name="job_title"  value="<?php echo $dataJobTitle;?>">
                <option>Default</option>
                <?php
                $conn=mysqli_connect('localhost','root','');
                $result=mysqli_query($conn,'SELECT id,job_title FROM hrm.job_title');
                while($row=mysqli_fetch_assoc($result)) {
                    echo "<option ".(($dataJobTitle == $row['job_title']) ? 'selected' : '')." value=". $row['job_title'].">". $row["job_title"]."</option>";
                }
                ?>
            </select>
        </div>


        <div>
            <p align="right">
                <button type="submit" class="btn btn-primary" ><?php echo $btnName; ?></button>
                <script>
                    let password = document.getElementById("password"),
                         confirm_password = document.getElementById("confirm_password");

                    function validatePassword(){
                        if(password.value != confirm_password.value) {
                            confirm_password.setCustomValidity("The specified  passwords do not match");
                        } else {
                            confirm_password.setCustomValidity('');
                        }
                    }

                    password.onchange = validatePassword;
                    confirm_password.onkeyup = validatePassword;
                </script>
                <button type="button" class="btn btn-secondary" onclick="history.back()">Back</button></p></div>
    </form>

    <br>
    <label for="status">Project_Name:</label>
    <select class="custom-select custom-select-sm"  id="project_name" name="project_name" ><option value='0'>Default</option>


        <br>
        <?php
        $conn=mysqli_connect('localhost','root','');
        $result=mysqli_query($conn,'SELECT id, project_name FROM hrm.project');
        while($row=mysqli_fetch_assoc($result)) {
            echo "<option value='". $row['id']."'>". $row['project_name']."</option>";
        }
        ?>
    </select>

    <br>
    <p align="right"><br><input type = "button" class="btn btn-primary" value="Add" id="add_project_btn"></inputbutton></p>


    <br>
    <div class="container">
        <table class="table table-bordered" id="project_dtl_list">
            <tbody id="table">
            <?php
            if (isset($_GET['id'])) {
            $conn=mysqli_connect('localhost','root','');
            $result=mysqli_query($conn,'SELECT project_id, project_name FROM hrm.user_project a left join hrm.project b on a.project_id=b.id where user_id =' .$dataId );
            while($row=mysqli_fetch_assoc($result)) {

                echo "<tr data-id= '" . $row['project_id'] . "' data-value='" . $row['project_name'] . "' id=\"" . $row['project_id'] . "\">
                <td data-id='" . $row['project_id'] . "'>" . $row['project_name'] . " <p align=\"right\">
            <button data-id=\"" . $row['project_id'] . "\" class=\"project_del_btn btn btn-outline-secondary btn-sm\">Delete</button></p></td> </tr>";
            }
            }
            ?>

            </tbody>
        </table>

    </div>
    <br>
<?php
include('../footer.php');
?>