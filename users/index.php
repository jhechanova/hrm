<?php
include('../header.php');
?>
<?php
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'user';
// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<br>
<div class="container">
    <div class="form-group">
    <h1>List of Users</h1>
    <table class="table table-bordered table-hover" align="center">
        <div class="form-group">
            <thead>
            <p align="center">
            <th scope="col" width="5%">ID#</th>
            <th scope="col" align="center">First Name</th>
            <th scope="col" align="center">Last Name</th>
            <th scope="col" align="center">Username</th>
            <th scope="col" align="center">E-mail</th>
            </thead>
        </div>
        <?php
        $sql = "SELECT * FROM hrm.user";
        $result = $conn->query($sql);
        if ($result && $result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "<tr><td align='center'><a href='user_form.php?id=".$row['id']."&parent=".$_GET['parent']."&level=".$_GET['level']."&tab_id=".$_GET['tab_id']."'>". $row["id"]."</a></td>";
                echo "<td>" .$row["first_name"]. "</td> ";
                echo "<td>" . $row["last_name"]. "</td>";
                echo "<td>". $row["username"]."</td>";
                echo "<td>". $row["email"]."</td>";"</tr>";
            }
        } else {
            echo "0 results";
        }
        $conn->close();
        ?>
        <div class="form-group">
            <p align="right">
        <a href="user_form.php">
            <button type="button" class="btn btn-primary" value="Add New Account" name="update">Add New Account</button></a>
            </p>
        </div>
    </table>
    </div>
</div>
<?php
include('../footer.php');
?>