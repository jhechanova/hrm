<?php
include('../header.php');
?>
<?php
$servername = 'localhost';
$username = 'root';
$password = '';
$dbname = 'hrm';
$table = 'project';

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<br>
<div class="container">
    <h1><p align="left">List of Projects</p></h1>
    <p align="right"><a href="project_form.php"><input type="submit"  class="btn btn-primary" value="Add New Project" name="New"></a></p></th>


    <table class="table table-bordered table-hover">
    <tr>
        <th scope="col" width="5%">ID#</th>
        <th scope="col" width="28%">Project Name</th>
        <th scope="col">Project Description</th>
        <th scope="col" width="8%">Status</th>
    </tr>
    <?php
    $sql = "SELECT * FROM hrm.project";
    $result = $conn->query($sql);
    if ($result ->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo "<tr><td align='center'><a href ='project_form.php?id=".$row['id']."&parent=".$_GET['parent']."&level=".$_GET['level']."&tab_id=".$_GET['tab_id']."'>". $row["id"]."</a></td>";
            echo "<td>" .$row["project_name"]. "</td> ";
            echo "<td>" . $row["project_description"]. "</td>";
            echo "<td>". $row["status"]."</td>";"</tr>";
        }
    } else {
        echo "0 results";
    }
    $conn->close();
    ?>
</table>
</div>
<?php
include('../footer.php');
?>