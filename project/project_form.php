<?php
include ('../header.php');

$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'project';
// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>

<?php

$dataId = '';
$dataProjectName = '';
$dataProjectDescription = '';
$btnName = 'Save';
$formLabel = 'New';
$dataStatus = 'Active';

if (isset($_GET['id'])) {

    $sql = "SELECT * FROM hrm.project where id = ". $_GET['id'];
//var_dump($result);
    $result = $conn->query($sql);
    $data = mysqli_fetch_row($result);
    $dataId = $data['0'];
    $dataProjectName = $data['1'];
    $dataProjectDescription = $data['2'];
    $btnName = 'Update';
    $dataStatus = $data['3'];
    $formLabel = 'Edit';
}
?>
<br>

<div class = "container">
    <form id="project_update_form" data-parent="<?php echo $_GET['parent'];?>" data-level="<?php echo $_GET['level'];?>" data-tab_id="<?php echo $_GET['tab_id'];?>">
    <h1><p align="left"><?= $formLabel?>Form</p></h1><br>
    <input type="hidden" required id="dataId" name="dataId" value="<?php echo $dataId;?>"/>
        <div class="form-group">
            <label for="project_name">Project Name:</label>
            <input type="text" class="form-control" placeholder="Enter Project Name" autofocus required id="project_name" name="project_name" value="<?php echo $dataProjectName;?>"/>
        </div>
        <div class="form-group">
            <label for="project_description">Project Description:</label>
            <input type="text" class="form-control" placeholder="Enter Project Description" required id="project_description" name="project_description" value="<?php echo $dataProjectDescription;?>"/>
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <select class="custom-select custom-select-sm" id ="status" name="status">
                <option <?= ($dataStatus == 'Active') ? 'selected' : '' ?> value="Active">Active</option>
                <option <?= ($dataStatus == 'Inactive') ? 'selected' : '' ?> value="Inactive">Inactive</option>
            </select>
        </div>
        <p align="right">
            <button type="submit" class="btn btn-primary"><?php echo $btnName; ?></button>
            <input type="button" class="btn btn-secondary" value="Back" onclick="history.back()"></p>
        <br>
    </form>
    <table class="table table-bordered" id="tasks_dtl_list">
        <label for="tasks_name">Select Task:</label>
        <select class="custom-select custom-select-sm" id ="tasks_name" name="tasks_name">
            <option value='0'>--Select--</option>
        <?php
        $conn=mysqli_connect('localhost','root','');
        $result=mysqli_query($conn,'SELECT id, tasks_name FROM hrm.task');
        while($row=mysqli_fetch_assoc($result)) {
            echo "<option value=". $row['id'].">". $row['tasks_name']."</option>";
        }
        ?>
        </select>
        <br>
        <br>

        <p align="right">
        <button type="submit" class="btn btn-primary btn-sm" value="add" id="add_task_btn">Add</button>
        </p>

        <div class="container">
            <table class="table table-bordered"  id="tasks_dtl_list">
    <tbody>
            <?php
            if (isset($_GET['id'])) {
            $conn = mysqli_connect('localhost','root','');
            $result = mysqli_query($conn,'SELECT task_id, tasks_name FROM hrm.project_detail a left join hrm.task b on a.task_id =b.id where project_id = '.$dataId);
            while($row = mysqli_fetch_assoc($result)) {
                echo "<tr data-id=" . $row['task_id'] . " data-value=" . $row['tasks_name'] . " id=" . $row['task_id'] . ">
                <td data-id=" . $row['task_id'] . ">" . $row['tasks_name'] . " 
                <p align=\"right\">
            <button data-id=" . $row['task_id'] . " class=\"tasks_del_btn btn btn-outline-secondary btn-sm\">Delete</button></p></td> </tr>";
            }
            }
            ?>
    </tbody>
    </table>
</div>
    </table>
</div>
    <br>
    <br>
<?php
include ('../footer.php');
?>

