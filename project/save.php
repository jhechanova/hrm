<?php

$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'project';

// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<?php

$project_name = $_POST['project_name'];
$project_description = $_POST['project_description'];
$status = '';
$status = $_POST['status'];

$taskDtl = json_decode($_POST['task_dtl']);

$sql = "Insert into hrm.project (project_name, project_description,status)
values ('$project_name', '$project_description', '$status')";

if ($conn->query($sql) === TRUE) {

    foreach ($taskDtl as $task) {
        $insSql = 'insert into hrm.project_detail (project_id, task_id) values ('.$conn->insert_id.', '.$task->id.')';
        $conn->query($insSql);
    }

    $conn->close();
    echo JSON_encode(array(
        'status'=> 'ok'
    ));

} else {
    $conn->close();
    echo JSON_encode(array(
        'status'=> 'fail'
    ));
}
?>