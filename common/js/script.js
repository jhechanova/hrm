$(document).ready(function () {
    let task = [];
    let project = [];
    let calendarQString = '';
    //task

    // $("#task_update_form").on('submit', function () {
    //     let url = 'add.php';
    //     let data = {
    //         'tasks_name': $('#tasks_name').val(),
    //         'tasks_description': $('#tasks_description').val(),
    //         'status': $('#status').val()
    //     };
    //     if ($('#dataId').val() != '') {
    //         url = 'update.php';
    //         data ['dataId'] = $('#dataId').val();
    //     }
    //     console.log($(this).data('parent'), $(this).data('level'),$(this).data('tab_id'));
    //     $.ajax({
    //         type: "POST",
    //         url: url,
    //         data: data,
    //         success: function (msg) {
    //             let data = JSON.parse(msg);
    //             if (data.status == 'ok') {
    //                 alert('Success');
    //                 window.location = 'index.php?&parent='+$('#task_update_form').data('parent')+'&level='+$('#task_update_form').data('level')+'&tab_id='+$('#task_update_form').data('tab_id');
    //             } else {
    //                 alert('Fail');
    //             }
    //         }
    //     });
    //     return false;
    // });


    //user_save
    $("#user_edit_form").on('submit', function () {
        if($('#username').val().indexOf(' ') >=0){
            console.log(" username contains space");
                alert('Invalid Username !');
            $(function() {
                $("#username").focus();
            });
                return false;
        }
        console.log(project);
        let url = 'save.php';
        let data = {
            'first_name': $('#first_name').val(),
            'last_name': $('#last_name').val(),
            'username': $('#username').val(),
            'email': $('#email').val(),
            'password': $('#password').val(),
            'phone_number': $('#phone_number').val(),
            'job_title': $('#job_title').val(),
            'project_dtl': JSON.stringify(project)
        };

        if ($('#dataId').val() != '') {
            url = 'update.php';
            data['dataId'] = $('#dataId').val()

        }


//user_update
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    alert('Success');
                    window.location = 'index.php?&parent='+$('#user_edit_form').data('parent')+'&level='+$('#user_edit_form').data('level')+'&tab_id='+$('#user_edit_form').data('tab_id');
                } else {
                    alert('Fail');
                }
            },
            fail: function (msg) {
                alert(msg)

            }

        });
        return false;

    });


    //project
    $("#project_update_form").on('submit', function () {
        let url = 'save.php';
        let data = {
            'project_name': $('#project_name').val(),
            'project_description': $('#project_description').val(),
            'status': $('#status').val(),
            'task_dtl': JSON.stringify(task)

        };
        if ($('#dataId').val() != '') {
            url = 'update.php';
            data['dataId'] = $('#dataId').val();
        }

        JSON.stringify('tasks_name');
        console.log($(this).data('parent'), $(this).data('level'),$(this).data('tab_id'));
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    alert('Success');
                    window.location = 'index.php?&parent='+$('#project_update_form').data('parent')+'&level='+$('#project_update_form').data('level')+'&tab_id='+$('#project_update_form').data('tab_id');
                } else {
                    alert('Fail');
                }

            }
        });
        return false;
    });

    //job title
    $("#job_update_form").on('submit', function () {
        let url = 'save.php';
        let data = {
            'job_title': $('#job_title').val(),
            'job_description': $('#job_description').val(),
            'status': $('#status').val()
        };
        if ($('#dataId').val() != '') {
            url = 'update.php';
            data ['dataId'] = $('#dataId').val();
        }
        console.log($(this).data('parent'), $(this).data('level'),$(this).data('tab_id'));
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    alert('Success');
                    window.location = 'index.php?&parent='+$('#job_update_form').data('parent')+'&level='+$('#job_update_form').data('level')+'&tab_id='+$('#job_update_form').data('tab_id');
                } else {
                    alert('Fail');
                }


            }


        });
        return false;
    });
    //time
    $("#time_edit_form").on('submit', function () {
        let url = 'save.php';
        let data = {
            'user_id': $('#user_id').val(),
            'log_date': $(`#log_date`).val(),
            'normal_hours': $('#normal_hours').val(),
            'ot_hours': $('#ot_hours').val(),
            'project_id': $('#project_id').val(),
            'task_id': $('#task_id').val(),
            'remarks': $('#remarks').val()
        };
        if ($('#dataId').val() != '') {
            url = 'update.php';
            data ['dataId'] = $('#dataId').val();
        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    alert('Success');
                    window.location = 'index.php';
                } else {
                    alert('Fail');
                }
            }
        });
        return false;
    });


    //add & delete button
    $('#add_task_btn').on('click', function() {
        if ($('#tasks_name').val() == 0) {
            alert("Please Select !");
            return;
        }
            var isFound = false;
            $.each(task,  function(i,o) {
                if($('#tasks_name').val() == o.id) {
                    isFound = true;
                }
            });

            if(isFound) {
                alert('Option is already selected !');
                return;
            }
            
        task.push(
            {
                "id": $('#tasks_name').val(),
                "tasks_name": $('#tasks_name option:selected').text(),
            });

        $('#tasks_dtl_list tbody').append('<tr id="'+ $('#tasks_name').val()+'"><td data-id="'+ $('#tasks_name').val() + '">' +
            '' + $('#tasks_name option:selected').text() + '' +
            '<p align="right">' + '<button data-id="'+ $('#tasks_name').val()+'" class="tasks_del_btn btn btn-secondary btn-sm" value="delete">Delete</button>' +
            '</p></td></tr>');

        addDelTaskEvent();


    });

    addDelTaskEvent();

    $.each($('#tasks_dtl_list tbody').find('tr'), function(i, e) {
        task.push(
            {
            'id': $(e).data('id'),
            'tasks_name': $(e).data('value')
        });
    });

    function addDelTaskEvent() {
        $('.tasks_del_btn').click('click', function(e) {
            $(this).closest('tr').remove();

            var row = $(this).data('id');

            $('tr#'+ $(this).data('id')).remove();
            var delId = $(this).data('id');

            task = $.grep(task, function (e) {
                console.log(e, delId);
                return e.id != delId;
            });
                
        });
    }

    $('.calendar-item').on('click', function() {
        $('#exampleModal').modal('show');

        calendarQString = $(this).data('qstring');
    });

    $('.project-item').on('click', function() {
        window.location = $(this).data('url') + '&' + calendarQString;
    });
    $('.project-item').on('change', function() {
        $('#project_id').find('project_id');

    });

    $('#add_project_btn').on('click', function () {
        if ($('#project_name').val() == 0) {
            alert("Please Select !");

return;
    }
        var isFound = false;
        $.each(project, function(i, o) {
            if($('#project_name').val()==o.id){
                isFound = true;

            }
        });
if(isFound){
    alert('Option is already selected !');
    return;
}


        project.push(
            {
                'id': $('#project_name').val(),
                'project_name': $('#project_name option:selected').text(),

            });

        $('#project_dtl_list tbody').append('<tr id="' + $('#project_name').val() + '"><td data-id="' + $('#project_name').val() + '">' + $('#project_name option:selected').text() + '' +
            '<p align="right">' +
            '<button data-id="' + $('#project_name').val() + '" class="project_del_btn btn btn-secondary btn-sm" value="delete">Delete</button></p></td></tr>');




        addDelEvent();
    });
    $.each($('#project_dtl_list tbody').find('tr'), function (i, e) {
        project.push(
            {
                'id': $(e).data('id'),
                'project_name': $(e).data('value')
            });

        addDelEvent();
    });

    $('#project_id').on('change', function () { $('#task_id').empty().append('<option value="">--Select--</option>');
        if($('#project_id').val()== '') return;
        let project_id = $('#project_id').val();

        $.ajax({
            url:'file.php',
            data: {'project_id':project_id,},
            type: 'POST',
        success: function (output) {
            output = JSON.parse(output);
            if (output.status !== 'error') {
                $.each(output.data, function (i,o) {
                    $('#task_id').append($('<option>', {
                        value: o.id,
                        text: o.tasks_name
                    }, '</option>'));
                });
            }
        }

        });

    });

    if ($('#project_id').length && $('#project_id').val()!= '') {
        let project_id = $('#project_id').val();
        let task_id = $('#task_id').data('value');
        $.ajax({
            url: 'file.php',
            data: {'project_id': project_id},
            type: 'POST',
            success: function (output) {
                $('#task_id').empty().append('<option value="">--Select--</option>');
                output = JSON.parse(output);
                if (output.status !== 'error') {
                    $.each(output.data, function (i, o) {
                        $('#task_id').append($('<option>', {
                                value: o.id,
                                text: o.tasks_name
                            }, '</option>')
                        );
                    });

                    $('#task_id').val(task_id);
                }
            }
        });
    }

    $('#monthly_generate_btn').on('click', function() {
        console.log($('#from').val());
        if($('#from').val()== 0 || $('#to').val()== 0 ) {
            alert('required to fill up');
          return;
        }
        window.location ='all_project_monthly_report.php?from='+ $('#from').val() + '&to=' + $('#to').val();

    });
    $('#generate_all_employee_monthly_btn').on('click', function() {
        console.log($('#all_employee_from').val());
        if($('#all_employee_from').val()== 0 || $('#all_employee_to').val()== 0 ) {
            alert('required to fill up');
            return;
        }
        window.location ='all_employee_monthly_report.php?from='+ $('#all_employee_from').val() + '&to=' + $('#all_employee_to').val();

    });


    $('#generate_employee_btn').on('click', function() {

        if($('#employee_from').val()== 0 || $('#employee_to').val()== 0 || $('#employee_opt').val() == '') {
            alert('required to fill up');
            return;
        }
        window.location ='employee_monthly_report.php?from='+ $('#employee_from').val() + '&to=' + $('#employee_to').val() + '&employee=' + $('#employee_opt').val();

    });
    $('.all_project_monthly_report_list').on('click', function () {

        var from = $(this).data('from');
        var to = $(this).data('to');
        var project_id = $(this).data('project_id');

        $.ajax({
            url: 'file.php',
            data: {
                'from':from,
                'to':to,
                'project_id':project_id
            },
            type: 'POST',
            success: function (output) {
                output = JSON.parse(output);
                if (output.status === 'success'){
                    $('#all_project_monthly_report_content').empty();
                    $.each(output.data, function (i,o) {
                    $('#all_project_monthly_report_content').append(
                        $('<tr>' +
                            '<td>' + o.name + '</td>' +
                            '<td>' + o.normal_hours + '</td>'+
                            '<td>' + o.ot_hours + '</td>'+
                            '</tr>')
                   );
                    });
                    $('#exampleModal').modal('show');
                }
            },
        });
    });

    $('#generate_project_btn').on('click', function() {

        if($('#project_from').val()== 0 || $('#project_to').val()== 0 || $('#project_opt').val() == '') {
            alert('required to fill up');
            return;
        }

        window.location ='project_monthly_report.php?from='+ $('#project_from').val() + '&to=' + $('#project_to').val() + '&project=' + $('#project_opt').val();

    });
    $('#add_user_btn').on('click', function () {
        if ($('#user_id').val() == 0 ){
            alert('required');
            return;
        }
        let url = 'insert.php';
        let data = {
            'user_id':$('#user_id').val()
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    window.location = 'index.php';
                }
            }
        });
        return false;
    });
    $('#delete').on('click', function () {
        let url = 'delete.php';
        var user_id = $(this).data('user_id');
        $.ajax({
            type: "GET",
            url: url,
            data: {
                'user_id':user_id
            },
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    window.location = 'http://localhost/hrm_project/user_management/?page=user_management';
                }
            }
        });
        return false;
    });

    function addDelEvent() {
        $('.project_del_btn').click('click', function (e) {
            $(this).closest('tr').remove();


            let row = $(this).data('id');

            $('tr#' + $(this).data('id')).remove();
            let delId = $(this).data('id');

            project = $.grep(project, function (e) {
                console.log(e, delId);
                return e.id != delId;

            });
        });
    }

});


// from action table

function updateTask(){
        let url = 'add.php';
        let data = {
            'tasks_name': $('#tasks_name').val(),
            'tasks_description': $('#tasks_description').val(),
            'status': $('#status').val()
        };
        if ($('#dataId').val() != '') {
            url = 'update.php';
            data ['dataId'] = $('#dataId').val();
        }
        console.log($(this).data('parent'), $(this).data('level'),$(this).data('tab_id'));
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (msg) {
                let data = JSON.parse(msg);
                if (data.status == 'ok') {
                    alert('Success');
                    window.location = 'index.php?&parent='+$('#dataParent').val()+'&level='+$('#dataLevel').val()+'&tab_id='+$('#dataTabId').val();
                } else {
                    alert('Fail');
                }
            }
        });
}

function deleteTask(){
    alert('deleteTask');
}
