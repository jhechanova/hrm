<?php
include('../header.php');
?>
<br>
<br>
<div class="container">
    <select type="text" class="form-control-sm" required id="user_id" name="user_id">
        <option value="">--Select--</option>
        <?php
        $conn=mysqli_connect('localhost','root','');
        $result=mysqli_query($conn,'SELECT * FROM hrm.user a left join hrm.user_rights b on a.id=b.user_id where b.user_id is null');
        while($row=mysqli_fetch_array($result)) {
            echo "<option  value=" . $row['id'] . ">" . $row["first_name"] . ' ' . $row['last_name'] . "</option>";
        }
        ?>
    </select>
    &nbsp;&nbsp;&nbsp;
    <button type="button" id="add_user_btn" class="btn">Add User</button>
    <br><br>
    <h1>List of Users</h1>
    <table class="table table-bordered table-hover" align="center">
        <thead><th>Users Name</th></thead>
        <?php
        $conn = mysqli_connect('localhost','root','');
        $sql = "select user_id,b.first_name,b.last_name FROM hrm.user_rights a left join hrm.user b on a.user_id=b.id group by a.user_id";
        $result = $conn->query($sql);
        if ($result && $result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           echo "<tr><td><a href='list_of_tabs.php?user_id=".$row['user_id']."'>".$row['first_name'].' '.$row['last_name']."</a><p align=\"right\">
                <button id='delete' data-user_id=".$row['user_id']." class=\"tasks_del_btn btn btn-outline-secondary btn-sm\">Delete</button></p></td></tr>";
        }
        }
        ?>
        </div>
<?php
include('../footer.php');
?>

