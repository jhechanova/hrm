<?php
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'user_rights';
// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<?php
$user_id = $_GET['user_id'];

$sql = 'DELETE FROM hrm.user_rights where user_id='.$user_id;
if ($conn->query($sql) === TRUE) {
    $conn->close();
    echo json_encode(array(
        'status'=>'ok'
    ));
} else {
    $conn->close();
    echo json_encode(array(
        'status'=>'fail'
    ));
}
?>