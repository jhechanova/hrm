<html>
<?php
session_start();

$url = 'http://localhost/hrm_project/';
if (isset($_SESSION['login_user']) && (isset($_SESSION['username']))) {
    //header("location: index.php");
}
else {
    header("location: /hrm_project/login.php");
}
?>


<head>
    <link rel = "stylesheet" href="<?= $url ?>common/css/calendar.css">
    <link rel = "stylesheet" href="<?= $url ?>common/css/style.css">
    <link rel = "stylesheet" href="<?= $url ?>common/css/bootstrap.min.css">
    <script src = "<?= $url ?>common/js/jquery-3.3.1.min.js"></script>
    <script src = "<?= $url ?>common/js/bootstrap.min.js"></script>
    <script src = "<?= $url ?>common/js/script.js"></script>
</head>
<?php
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'tabs';

$conn = new mysqli($server_name, $username, $password, $db_name);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<body background="">
<nav class="navbar navbar-inverse navbar-dark bg-dark">
    <div class="nav nav-tabs" id="header">

<?php

       $page =(isset($_GET['page']) ?$_GET['page']: (isset($_SESSION['page'])?$_SESSION['page']:'home'));
        $_SESSION['page'] = $page;

        $sql = "SELECT * FROM hrm.tabs";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {

                echo '<a class="nav-item nav-link ' . (($page == str_replace(' ', "_", strtolower($row["name"]))) ? 'active' : '') . '" href =' . $row['action'] . '&parent='.$row['parent'].'&level='.$row['level'] .'&tab_id='.$row['id'].'>' . $row["name"] . '</a>';
            }
        } else {
            echo "0 results";
        }
        $conn->close();
?>
    </div>
    <p class = "col-lg-3" align = "right" ><a class="text-white-50 m-3"><?= $_SESSION['username'] ?></a><a class="btn btn-outline-primary <?php echo ($_GET['page'] == 'logout') ? 'active' : ''; ?>" href = "/hrm_project/logout.php" style = "text-decoration-none:" aria - selected = "false" > Logout</a ></p >
</nav>
