<?php
include('header.php');
?>
<br>
<br>
<?php
$conn = mysqli_connect('localhost','root','');
$month = 'month(now())';
$year = 'year(now())';

if (isset($_GET['month']) && isset($_GET['year'])) {
    $month = $_GET['month'];
    $year = $_GET['year'];
}
$sql = 'select sum(normal_hours) as TotalNormalHours, sum(ot_hours) as TotalOTHours from hrm.time where month(`log_date`)= '.$month.' and year(`log_date`)= '.$year.' and user_id=' . $_SESSION['login_user'];
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($result);
?>
<div align="center">
    <b>Total Normal Hours:</b>
    <?= $row['TotalNormalHours']?:'0.00' ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <b>Total OT Hours:</b>
    <?= $row['TotalOTHours']?:'0.00' ?>
</div>
<br>
<?php
include 'calendar_en.php';
$calendar = new Calendar();
echo $calendar->show();


?>

<?php
include('footer.php');
?>
<script>
        history.pushState(null, null, null);
        window.addEventListener('popstate', function () {
        history.pushState(null, null, null);
    });
</script>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="col modal-title text-center"><b>Projects</b></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <table class="table table-bordered" style= "width: 100%" >
                    <tr><td width="232"><b><h5 align="center"> User Project List: </h5></b></td><td align="center">Total Normal Hours</td><td align="center">Total O.T Hours</td></tr>

                    <tbody>

                    <?php
                    $conn = mysqli_connect('localhost','root','');
                    $result = mysqli_query($conn,'SELECT user_id,  project_name, project_id  FROM hrm.user_project a left join hrm.project b on a.project_id =b.id where  a.user_id = ' . $_SESSION['login_user']);
                    $dataId = '';

                    $projectIds = [];

                    while ($row = mysqli_fetch_assoc($result)) {
                        $month = 'month(now())';
                        $year = 'year(now())';
                        if(isset($_GET['month']) && isset($_GET['year'])){
                            $month = $_GET['month'];
                            $year = $_GET['year'];
                        }
                        $conn = mysqli_connect('localhost','root','');
                        $result1 = mysqli_query($conn, 'select user_id, sum(normal_hours) as totalNormalhours, sum(ot_hours) as totalOthours from hrm.time where month(log_date)= '.$month.' and year(log_date)= '.$year.' and user_id =  ' . $_SESSION['login_user']  .' and project_id = '.$row['project_id'] ); //var_dump($result1);
                        $row2 = mysqli_fetch_assoc($result1); //var_dump($row2);

                        $projectIds[] = $row['project_id'];

                        $dataId = '?project_id=' . $row['project_id'] . '&user_id=' . $_SESSION['login_user'];
                        echo "<tr data-id=" . $row['project_id'] . " data-value=" . $row['project_name'] . " id=\"" . $row['project_name'] . "\" ></tr> 
                          <tr><td><a class='project-item' role='button' href='#' style= 'text-decoration:none'  data-url='http://localhost/hrm_project/time/time_form.php" . $dataId . "'> " . $row['project_name']. "</a><br><br>
                          </td><td align='center'> ".(!is_null($row2['totalNormalhours'])?$row2['totalNormalhours']: '0.00')."  </td><td align='center'>".(!is_null($row2['totalOthours'])?$row2['totalOthours']: '0.00')." </td></tr>";
                    }
                    ?>

                    </tbody>
                </table>
                <style>
                    hr{
                        width: 465px;
                        height: 1px;
                    }
                </style>

                <tr><td><hr color="black"></td></tr>


                <table class="table table-bordered" style="width: 100%">
                    <tr><td width="232"><b><h5 align="center"> All Project List: </h5></b></td><td align="center">Total Normal Hours</td><td align="center">Total O.T Hours</td></tr>
                    <style>
                        .table-bordered{
                            width: 230px;
                        }
                    </style>
                    <?php
                    $conn = mysqli_connect('localhost','root','');
                    $result = mysqli_query($conn, 'Select id,  project_name from hrm.project where project.id not in (' . implode(',', $projectIds) . ') ');
                    while($row1=mysqli_fetch_assoc($result)) {
                        $month1 = 'month(now())';
                        $year1 = 'year(now())';
                        if(isset($_GET['month']) && isset($_GET['year'])){
                            $month1 = $_GET['month'];
                            $year1 = $_GET['year'];
                        }

                        $conn = mysqli_connect('localhost','root','');
                        $result2 = mysqli_query($conn, 'select user_id, sum(normal_hours) as totalNormalhours, sum(ot_hours) as totalOthours from hrm.time where month(log_date)= '.$month1.' and year(log_date)= '.$year1.' and user_id =  ' . $_SESSION['login_user']  .' and project_id = '.$row1['id'] );
                        $row3 = mysqli_fetch_assoc($result2); //var_dump($row3);


                        $dataId2 = '?project_id=' . $row1['id'] . '&user_id=' . $_SESSION['login_user'] ;

                        echo "<tr data-id=" . $row1['id'] ."data-value =" .$row1['project_name'] . " id=\"" . $row1['project_name'] . "\">
                          <td><a class='project-item' role='button' href='#' style='text-decoration:none' data-url='http://localhost/hrm_project/time/time_form.php" . $dataId2 . "'> " . $row1['project_name'] . "</a>
                          <br><br>
                          </td><td align='center'> ".(!is_null($row3['totalNormalhours'])?$row3['totalNormalhours']: '0.00')."   </td><td align='center'> ".(!is_null($row3['totalOthours'])?$row3['totalOthours']: '0.00')."  </td></tr>";
                    }
                    ?>

                </table>
            </div>
        </div>
    </div>
</div>
