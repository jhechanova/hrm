<?php
include('../header.php');
?>
<br>
<br>
<div class="container">
    <div class="form-group">
        <table class="table table-bordered table-hover" align="center">
            <div class="form-group">
                <thead>
                <p align="center">
                <th scope="col" align="center">Project Name</th>
                <th scope="col" align="center">Normal Hours</th>
                <th scope="col" align="center">OT Hours</th>
                <th scope="col" width="5%">Action</th>
                </thead>
            </div>
            <?php
            $conn=mysqli_connect('localhost','root','');
            if (isset($_GET['from']) && isset($_GET['to'])){
                $from = $_GET['from'];
                $to = $_GET['to'];

            }
            $sql = "select sum(normal_hours) as TNH , sum(ot_hours) as TOtH, b.project_name, a.project_id from hrm.time a left join hrm.project b on a.project_id=b.id where log_date >='".$from."' and log_date <= '".$to."' group by a.project_id";
            $result= mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<tr><td>'.$row['project_name'].'</td>';
                echo '<td>'.$row['TNH'].'</td>';
                echo '<td>'.$row['TOtH'].'</td>';
                echo "<td class='all_project_monthly_report_list' data-from='$from' data-to='$to' data-project_id='".$row['project_id']."'><a href='#'>view</a></td></tr>";
            }
            ?>
        </table>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="col modal-title text-center" id="exampleModalLabel">&nbsp;&nbsp;&nbsp;&nbsp;Employee's Name</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Employee Name</th>
                                <th>Normal Hours</th>
                                <th>OT Hours</th>
                            </tr>
                        </thead>
                        <tbody id="all_project_monthly_report_content">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include('../footer.php');
?>
