<?php
include('../header.php');
?>
<br>
<br>
<li><a href="#" type="text" class="text-black-100" data-toggle="modal" data-target="#monthly">All Project Monthly Report</a></li>
    <div class="modal fade" id="monthly" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="col modal-title text-center">All Project Monthly Report</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="from">From:</label>
                        <input type="date" class="form-control" required id="from" name="from" value=""/>
                    </div>
                    <div class="form-group">
                        <label for="to">To:</label>
                        <input type="date" class="form-control" required id="to" name="to" value=""/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="monthly_generate_btn" class="btn btn-primary">Generate</button>
                </div>

            </div>
        </div>
    </div>
<br>

<li><a href="#" type="text" class="text-black-100" data-toggle="modal" data-target="#all_employee_monthly">All Employee Monthly Report</a></li>
<div class="modal fade" id="all_employee_monthly" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="col modal-title text-center">All Employee Monthly Report</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="from">From:</label>
                    <input type="date" class="form-control" required id="all_employee_from" name="all_employee_from" value=""/>
                </div>
                <div class="form-group">
                    <label for="to">To:</label>
                    <input type="date" class="form-control" required id="all_employee_to" name="all_employee_to" value=""/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="generate_all_employee_monthly_btn" class="btn btn-primary">Generate</button>
            </div>

        </div>
    </div>
</div>
<br>
<li><a href="#" type="text" class="text-black-100" data-toggle="modal" data-target="#employee">Employee Monthly Report</a></li>
<div class="modal fade" id="employee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="col modal-title text-center">Employee Monthly Report</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="from">From:</label>
                    <input type="date" class="form-control" required id="employee_from" name="employee_from" value=""/>
                </div>
                <div class="form-group">
                    <label for="to">To:</label>
                    <input type="date" class="form-control" required id="employee_to" name="employee_to" value=""/>
                </div>
                <div class="form-group">
                    <label for="employee">Employee:</label>
                    <select type="text" class="form-control " required id="employee_opt" name="employee_opt"  value="">
                        <option value="">--Select--</option>
                        <?php
                        $conn=mysqli_connect('localhost','root','');
                        $result=mysqli_query($conn,'SELECT * FROM hrm.user');
                        while($row=mysqli_fetch_array($result)) {

                        echo "<option  value=" . $row['id'] . ">" . $row["first_name"] . ' ' . $row['last_name'] . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" id="generate_employee_btn" class="btn btn-primary">Generate</button>
            </div>

        </div>
    </div>
</div>
<br>
<li><a href="#" type="text" class="text-black-100" data-toggle="modal" data-target="#project">Project Monthly Report</a></li>
<div class="modal fade" id="project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="col modal-title text-center">Project Monthly Report</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="from">From:</label>
                    <input type="date" class="form-control" required id="project_from" name="project_from" value=""/>
                </div>
                <div class="form-group">
                    <label for="to">To:</label>
                    <input type="date" class="form-control" required id="project_to" name="project_to" value=""/>
                </div>
                <div class="form-group">
                    <label for="project">Project:</label>
                    <select type="text" class="form-control " required id="project_opt" name="project_opt"  value="">
                        <option value="">--Select--</option>
                        <?php
                        $conn=mysqli_connect('localhost','root','');
                        $result=mysqli_query($conn,'SELECT * FROM hrm.project');
                        while($row=mysqli_fetch_array($result)) {

                            echo "<option value=" . $row['id'] . ">" . $row["project_name"] . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="generate_project_btn" class="btn btn-primary">Generate</button>
            </div>

        </div>
    </div>
</div>


<?php
include('../footer.php');
?>

