<?php
include('../header.php');
?>
<br>
<br>
<div class="container">
    <div class="form-group">
        <table class="table table-bordered table-hover" align="center">
            <div class="form-group">
                <thead>
                <p align="center">
                <th scope="col" align="center">Employee Name</th>
                <th scope="col" align="center">Normal Hours</th>
                <th scope="col" align="center">OT Hours</th>
                <th scope="col" align="center" width="5%"> Action </th>
                </thead>
            </div>
            <?php
            $conn=mysqli_connect('localhost','root','');
            if (isset($_GET['from']) && isset($_GET['to'])){
                $from = $_GET['from'];
                $to = $_GET['to'];
            }
            $sql = "select sum(normal_hours) as TNH , sum(ot_hours) as TOtH, concat(b.first_name,' ', b.last_name) as employee_name,a.user_id from hrm.time a left join hrm.user b on a.user_id=b.id where log_date >='".$from."' and log_date <= '".$to."' group by a.user_id";
            $result= mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<tr><td>'.$row['employee_name'].'</td>';
                echo '<td>'.$row['TNH'].'</td>';
                echo '<td>'.$row['TOtH'].'</td>';
                echo "<td class='all_employee_monthly_report_list' data-from='$from' data-to='$to' data-user_id='".$row['user_id']."'><a href='#'>view</a></td></tr>";
            }
            ?>
        </table>
    </div>
    <div class="modal fade" id="exampleModal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="col modal-title text-center" id="exampleModalLabel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Projects</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Normal Hours</th>
                            <th>OT Hours</th>
                        </tr>
                        </thead>

                        <tbody id="all_employee_monthly_report_content">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include('../footer.php');
?>
