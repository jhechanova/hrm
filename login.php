
<?php
$url = 'http://localhost/hrm_project/';
?>

<head>
    <title>Login Page</title>
    <link rel = "stylesheet" href="<?= $url ?>common/css/calendar.css">
    <link rel = "stylesheet" href="<?= $url ?>common/css/style.css">
    <link rel = "stylesheet" href="<?= $url ?>common/css/bootstrap.min.css">
    <script src = "<?= $url ?>common/js/jquery-3.3.1.min.js"></script>
    <script src = "<?= $url ?>common/js/bootstrap.min.js"></script>
    <script src = "<?= $url ?>common/js/script.js"></script>


    <script>
        history.pushState(null, null, null);
        window.addEventListener('popstate', function () {
            history.pushState(null, null,);
        });

    </script>
</head>

<?php
define('server_name', 'localhost');
define('username', 'root');
define('password', '');
define('db_name', 'hrm');
$db = mysqli_connect(server_name,username,password, db_name);
?>



<?php

session_start();
$error = '';
if (isset($_SESSION['login_user']) && (isset($_SESSION['username']))) {
    header("location: /hrm_project/index.php");
}
if($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = mysqli_real_escape_string($db,$_POST['username']);
    $password = mysqli_real_escape_string($db,$_POST['password']);

    $sql = "SELECT id, first_name, last_name FROM user WHERE username = '$username' and `password` = password('$password')";
    $result = mysqli_query($db,$sql);
    $row = mysqli_fetch_array($result);

    $count = mysqli_num_rows($result);
    if($count == 1 ) {
        $_SESSION['login_user'] = $row['id'];
        $_SESSION['username'] = $row['first_name'].' '.$row['last_name'];

        header("location: /hrm_project/index.php");

    }else {
        $error = "Your UserName or Password is invalid";
    }
}
?>
<br>
<head>
    <style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
    input[type=text], input[type=password]
    {
    width: 100%;
    padding: 12px 20px;
    margin: 5px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
    }
    h1{
    font-weight: bolder;
    }
    .btn-block:hover {
        background-color: forestgreen;
        color: white;
    }
    #text {display:none;color: #f3000d
    }
    </style>
</head>
<br>
<div align = "center">
    <div style = "width:550px" align = "left" >

        <h1 align="center"><br>Login Form</h1><br>
        <br>
        <br>
        <?php
        if (!empty($error)) {
            ?>
            <div class="alert alert-warning" role="alert">
                <strong>Invalid username and password!</strong>. Please try again.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
        }
        ?>
            <form method="post" action="login.php">
                <label>Username  :</label><input id="username" type = "text" placeholder="Enter User Name" name = "username" autofocus class = "form-control" required/><br /><br />


                <label>Password  :</label><input id="password" type = "password"  placeholder="Enter Password" name = "password" class = "form-control" required/><br/><br />
                <p id="text">WARNING! Caps lock is ON.</p>
                <script>
                    let input = document.getElementById("password");
                    let text = document.getElementById("text");
                    input.addEventListener("keyup", function(event) {

                        if (event.getModifierState("CapsLock")) {
                            text.style.display = "block";
                        } else {
                            text.style.display = "none"
                        }
                    });
                </script>

                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
                <p align="center"> <input type = "submit" id="user-id" class="btn btn-primary btn-block" data-dissmiss="alert" value="Login"></p>

            </form>
        </div>

        </div>
