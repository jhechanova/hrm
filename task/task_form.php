<?php
include('../header.php');
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'task';
$conn = mysqli_connect($server_name,$username,$password,$db_name);
?>
<?php
$dataId = '';
$dataTaskName = '';
$dataTaskDescription = '';
$btnName = 'Save';
$formLabel = 'New';
$dataStatus = 'Active';

try {
    if (isset($_GET['id'])) {

        $sql = "SELECT * FROM hrm.task where id = '" . $_GET['id'] . "' ";
        $result = mysqli_query($conn, $sql);
        $data = mysqli_fetch_row($result);
        $dataId = $data['0'];
        $dataTaskName = $data['1'];
        $dataTaskDescription = $data['2'];
        $dataStatus=$data['3'];
        $btnName = 'Update';
        $dataStatus = $data['3'];
        $formLabel = 'Edit';
    }
}catch (Exception $e){var_dump($e->getMessage());}
?>
<br>
 <div class="container">
<!--        <form id = "task_update_form" data-parent="--><?//= $_GET['parent']?><!--" data-level="--><?//= $_GET['level']?><!--" data-tab_id="--><?//= $_GET['tab_id']?><!--">-->
            <input type="hidden" id ="dataId" name="dataId" size="30" maxlength="30" value="<?php echo $dataId;?>"/>
     <input type="hidden" id ="dataParent" name="dataParent" size="30" maxlength="30" value="<?php echo $_GET['parent'];?>"/>
     <input type="hidden" id ="dataLevel" name="dataLevel" size="30" maxlength="30" value="<?php echo $_GET['level'];?>"/>
     <input type="hidden" id ="dataTabId" name="dataTabId" size="30" maxlength="30" value="<?php echo $_GET['tab_id'];?>"/>
                <h1><?= $formLabel ?> Form</h1>
            <br>
                <div class="form-group">
                <label for="tasks_name">Task Name:</label>
                    <input type="text" class="form-control" placeholder="Enter Task Name" autofocus required id = "tasks_name" name="tasks_name" value="<?php echo $dataTaskName;?>">
                </div>
                <div class="form-group">
                <label for="tasks_description">Task Description:</label>
                    <input type="text" class ="form-control" placeholder="Enter Task Description" required id = "tasks_description"  name="tasks_description" value="<?php echo $dataTaskDescription;?>">
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="custom-select custom-select-sm" id ="status" name="status">
                        <option <?= ($dataStatus == 'Active') ? 'selected' : '' ?> value="Active">Active</option>
                        <option <?= ($dataStatus == 'Inactive') ? 'selected' : '' ?> value="Inactive">Inactive</option>
                    </select>
                </div>
            <p align="right">
                <?php

                if(isset($_GET['tab_id']) && isset($_GET['parent']) && isset($_GET['level'])){
                    $tab_id = $_GET['tab_id'];
                    $p_tab = $_GET['parent'];
                    $l_tab = $_GET['level'];
                }


                $conn = mysqli_connect('localhost','root','');
                $result = mysqli_query($conn,"Select * from hrm.actions where tab_id='$tab_id' and p_tab='$p_tab' and l_tab='$l_tab' order by sort");
                while ($row=mysqli_fetch_assoc($result)) { //var_dump($row);


                    echo '<button onclick="'.$row['action'].'" data-tab_id="' . $row['tab_id'] . '" data-p_tab="'.$row['p_tab'].'" data-l_tab="'.$row['l_tab'].'"  class="btn btn-primary">'.$row['label'].' </button> ' ;

                }
                ?>

<!--                <button type="submit" class="btn btn-primary" name="update" value="update" >--><?//= $btnName ?><!--</button>-->
                <input type="button" class="btn btn-secondary" value="Back" onclick="history.back()"></p>

<!--        </form>-->
    </div>
<?php
include('../footer.php');
?>