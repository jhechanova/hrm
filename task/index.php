<?php
include('../header.php');
?>
<?php
$server_name = 'localhost';
$username = 'root';
$userpassword = '';
$db_name = 'hrm';
$table = 'task';
$conn = new mysqli($server_name,$username,$userpassword,$db_name);
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}
?>
<div class="container">
    <center><table class="table table-bordered table-hover">
            <thead class="thead">
            <p align="center">
            <th width="5%"> ID# </th>
            <th width="30%"> Tasks Name </th>
            <th width="70%"> Tasks Description </th>
            <th width="20%"> Status </th>
            <h1><p align="left">List of Tasks</p></h1>
            <p align="right"><a href="task_form.php?parent=<?= $_GET['parent']?> &level=<?= $_GET['level']?> &tab_id=<?= $_GET['tab_id']?>"><input type="button" class="btn btn-primary" name="update" value="Add New Task"></a></p>
            </thead>
            <tbody>
            <?php
            $sql = 'SELECT * FROM hrm.task ';
            $result = $conn->query($sql);
            if ($result && $result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr><td align='center'><a href ='task_form.php?id=".$row['id']."&parent=".$_GET['parent']."&level=".$_GET['level']."&tab_id=".$_GET['tab_id']."'>". $row["id"]."</a></td>";
                    echo "<td>" .$row["tasks_name"]. "</td> ";
                    echo "<td>" . $row["tasks_description"]. "</td>";
                    echo "<td>". $row["status"]."</td>";
                }
            } else {
                echo "0 results";
            }
            $conn->close();
            ?>
            </tbody>

            <a href="task_form.php?id=<?php echo $result["id"]; ?>"></a>

        </table>
    </center>
</div>
<?php
include('../footer.php');
?>
