<?php
include('../header.php');
?>
<?php
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'time';
$conn = new mysqli($server_name, $username, $password, $db_name);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<?php
$dataId = '';
$dataUser_id= '';
$dataLog_date = '';
$dataNormal_hours = '';
$dataOt_hours = '';
$dataProject_id = '';
$dataTask_id = '';
$dataRemarks = '';
$btnName = 'Save';
$formLabel = 'New';

if (isset($_GET['id'])) {
    $sql = "SELECT * FROM hrm.time where id = " . $_GET['id'];
    $result = $conn->query($sql);
    $data = mysqli_fetch_assoc($result);
    $dataId = $data['id'];
    $dataUser_id = $data['user_id'];
    $dataLog_date = $data['log_date'];
    $dataNormal_hours = $data['normal_hours'];
    $dataOt_hours = $data['ot_hours'];
    $dataProject_id = $data['project_id'];
    $dataTask_id = $data['task_id'];
    $dataRemarks = $data['remarks'];
    $btnName = 'Update';
    $formLabel = 'Edit';

}
if (isset($_GET['year']) && isset($_GET['month']) && isset($_GET['day'])){
    $dataLog_date = $_GET['year']. '-'.$_GET['month'].'-' .$s = sprintf('%02d', $_GET['day']);

}
if(isset($_GET['project_id']) && empty($dataProject_id)){
    $dataProject_id = $_GET['project_id'];
}

?>
    <div class="container"">
        <form id="time_edit_form">
            <input type="hidden" id = "dataId" name="dataId" value="<?php echo $dataId; ?>"/>
            <h1><?= $formLabel?> Form</h1>
            <br>
            <thead>
            <div class="form-group">
                <label for="from">Log Date:</label>
                <input type="date" class="form-control" required id="log_date" name="from" value="<?php echo $dataLog_date;?>"/>
            </div>
            <div class="form-group">
                <label for="normal_hours">Normal Hours:</label>
                <input type="text" class="form-control" required id="normal_hours" name="normal_hours" value="<?php echo $dataNormal_hours;?>"/>
            </div>
            <div class="form-group">
                <label for="ot_hours">OT Hours:</label>
                <input type="text" class="form-control" required id="ot_hours" name="ot_hours" value="<?php echo $dataOt_hours;?>"/>
            </div>
            <div class="form-group">
                <label for="project_id">Project:</label>
                <select type="text" class="form-control " required id="project_id" name="project_id"  value="<?php echo $dataProject_id;?>">
                <option value="">--Select--</option>
                <?php
                $conn=mysqli_connect('localhost','root','');
                $result=mysqli_query($conn,'SELECT * FROM hrm.project');
                while($row=mysqli_fetch_array($result)) {

                    echo "<option " . (($dataProject_id == $row['id']) ? 'selected' : '') . " value=" . $row['id'] . ">" . $row["project_name"] . "</option>";
                }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label for="task_id">Task:</label>
                <select type="text" class="form-control" required id="task_id" name="task_name" data-value="<?php echo $dataTask_id;?>">
                    <option value="">--Select--</option>
                </select>
            </div>

            <div class="form-group">
                <label for="remarks">Remarks:</label>
                <textarea type="text" class="form-control" required id="remarks" name="remarks" value="<?php echo $dataRemarks;?>"><?php echo $dataRemarks;?></textarea>
            </div>
            <div>
                <p align="right">
                    <button type="submit" class="btn btn-primary"><?php echo $btnName; ?></button></a>
                    <button type="button" class="btn btn-secondary" onclick="history.back()">Back</button></p></div>
        </form>
    </div>
    </thead>
<?php
include('../footer.php');
?>