<?php
include('../header.php');
?>
<?php
$server_name = 'localhost';
$username = 'root';
$password = '';
$db_name = 'hrm';
$table = 'time';
// Create connection
$conn = new mysqli($server_name, $username, $password, $db_name);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
    <br>
    <div class="container">
        <div class="form-group">
            <h1>List of Time</h1>
            <table class="table table-bordered table-hover" align="center">
                <div class="form-group">
                    <thead>
                    <th scope="col" align="center" width="5%">ID#</th>
                    <th scope="col" align="center" width="10%">Log Date</th>
                    <th scope="col" align="center" width="12%">Normal Hours</th>
                    <th scope="col" align="center"  width="9%">OT Hours</th>
                    <th scope="col" align="center">Project</th>
                    <th scope="col" align="center">Task</th>
                    <th scope="col" align="center">Remarks</th>
                    </thead>
                </div>
                <?php

                $conn = mysqli_connect('localhost','root','');
                $result = mysqli_query($conn, 'Select a.*, b.project_name, c.tasks_name from hrm.time a
                                                      left join hrm.project b on a.project_id = b.id
                                                      left join hrm.task c on a.task_id = c.id where a.user_id = '. $_SESSION['login_user']);

                if ($result && $result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        echo "<tr><td align='center'><a href ='time_form.php?id=".$row['id']."'>". $row['id']."</a></td>";
                        echo "<td>" . $row["log_date"]. "</td>";
                        echo "<td>". $row["normal_hours"]."</td>";
                        echo "<td>". $row["ot_hours"]."</td>";
                        echo "<td>". $row["project_name"]."</td>";
                        echo "<td>". $row["tasks_name"]."</td>";
                        echo "<td>". $row["remarks"]."</td>";
                        "</tr>";
                    }
                } else {
                    echo "0 results";
                }
                $conn->close();
                ?>
                <div class="form-group">
                    <p align="right">
                        <a href="time_form.php">
                            <button type="button" class="btn btn-primary" value="Add New Account" name="update">Add New Time</button></a>
                    </p>
                </div>
            </table>
        </div>
    </div>
<?php
include('../footer.php');
?>
